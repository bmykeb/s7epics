# S7EPICS

S7EPICS in an open source PLC code, implemented as a TIA Portal library, which enables EPICS high-level applications to connect directly to a Siemens S71500 family PLC without an actual EPICS IOC in between the two components. Consequently, it simplifies the control system as a whole and, eventually, helps reducing its costs and time to production.
The library is useful for a (research) group that do not possess much EPICS competences and/or have great PLC development expertise, and the business (control) logic is entirely implemented in the PLC.
To validate the S7EPICS library many tests were per-formed and these have successfully demonstrated that the library is compatible with generic CA clients (e.g., caput, caget, CS-Studio) and is capable of successfully handling (some) thousands of PVs without putting an unmanagea-ble load on the PLC.  

S7EPICS listens to UDP broadcast messages. According to my current knowledge, this connection can't be configured by a DB. 
So please check the example project Connections tab in the CPUs Network View. There is a UDP boradcast connection configured on the Local ID 101. Without this connection S7EPICS doesn't work.

Use the PLC example in TIA v14SP1 in the repo.

Contact me: borosmiklos@gmail.com


